const path = require('path');

const webpack = require('webpack');

const TerserJSPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

// Find the compile mode
let MODE = 'dev'
const modeIndex = process.argv.indexOf('--mode')
if (modeIndex !== -1 
        && (modeIndex + 1) < process.argv.length) {
    MODE = process.argv[modeIndex + 1];
}

// set minimizer for development and production mode
const minimizer = [
    new OptimizeCSSAssetsPlugin({}),
];

if (MODE === 'production') {
    minimizer.push(
        new TerserJSPlugin({
            sourceMap: true,
            extractComments: true,
        })
    );
}


module.exports = {
    mode: 'development',
    entry: './src/js/main.ts',
    output: {
        filename: 'js/bundle.js',
        path: path.resolve(__dirname, '../cmapplication/static/')
    },
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader',
                ]
            }
        ]
    },
    devtool: 'inline-source-map',
    plugins: [
        new MiniCssExtractPlugin({
            filename: "css/style.css", // use style.css instead of [name].css because must create only one file for all the application
            chunkFilename: "[id].css",
        }),
        new webpack.SourceMapDevToolPlugin({
            filename: 'js/[name].js.map',
            exclude: ['vendor.js'],
        }),
    ],
    optimization: {
        minimizer,
    },
    resolve: {
        extensions: ['.ts', '.js'],
    }
};

