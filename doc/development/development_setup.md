# Setup the development environment

This document is the procedure to install tools for this application 
development.

## Clone the project

On the GitLab competition-maker repository page click on "Clone" to create your 
local repository with the actual source code.

## Python for back-end

The application is developed for the back-end with python 3 and Django framework.  

### Python installation
* Install [Python 3](https://www.python.org/) on your device.
* Install [PyPI](https://pip.pypa.io/en/stable/installing/) to download 
  dependencies.
* install [virtualenv](https://virtualenv.pypa.io/en/latest/) package to keep 
  your installed package safe.

### Create a virtual environment

To prevent eny requirement conflict, use a python virtual environment.

```commandline
python3 -m venv env
```

### Download dependencies

When the virtual environment is initialized you can import dependencies.  
  
In the project root directory, after activating the virtual environment download 
dependencies with PyPI.

```commandline
pip install -r requirements.txt
```

> Remember to activate the virtual environment before starting to develop for 
> this project.

### Python IDE

I suggest to use [PyCharm](https://www.jetbrains.com/pycharm/download/#section=linux) 
community edition to develop, but any IDE can be chosen.

## Front-end

The application UI is created by the back-end server thanks to templates, but to 
facilitate the front-end development for CSS and JavaScript we use the webpack 
package.  
The Webpack package allows to create css from scss and JavaScript script from
TypeScript modules.

### Install Node.js and npm

To install *Node.js* and *npm* follow instruction on 
[official website](https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions-enterprise-linux-fedora-and-snap-packages).  
*npm* is the dependency downloader to use for your project.

### Install dependencies

From the project root directory move to the **ui-source** directory.

```commandline
cd ui-source
```

Is now possible to install front-end dependencies. Run the following command line :

```commandline
npm install
```

The front-end environment development is setup.  
Before starting to develop for the front, don't forgive to start the next command 
to auto build for development mode.

```commandline
npm run watch
```

This command allows to look *.ts and *.scss changes to compile the main *bundle.js* 
and *style.css* providing by Django server.

### Front-end IDE

I suggest to use [Visual Studio Code](https://code.visualstudio.com/) to develop 
front-end code, but any IDE can be chosen.
